"""
Nose tests for acp_times.py
"""

from acp_times import open_time, close_time


def test_simple_case():
    assert str(open_time(150, 200, "2017-01-01T00:00:00-08:00")) == "2017-01-01T04:25:00-08:00"
    assert str(close_time(150, 200, "2017-01-01T00:00:00-08:00")) == "2017-01-01T10:00:00-08:00"


def test_less_than_60km():
    assert str(open_time(20, 200, "2017-01-01T00:00:00-08:00")) == "2017-01-01T00:35:00-08:00"
    assert str(close_time(20, 200, "2017-01-01T00:00:00-08:00")) == "2017-01-01T02:00:00-08:00"


def test_zero_input():
    assert str(open_time(0, 200, "2017-01-01T00:00:00-08:00")) == "2017-01-01T00:00:00-08:00"
    assert str(close_time(0, 200, "2017-01-01T00:00:00-08:00")) == "2017-01-01T01:00:00-08:00"


def test_large_input():
    assert str(open_time(500, 200, "2017-01-01T00:00:00-08:00")) == "Invalid input!"
    assert str(close_time(500, 200, "2017-01-01T00:00:00-08:00")) == "Invalid input!"


def test_invalid_input():
    assert str(open_time(-1, 200, "2017-01-01T00:00:00-08:00")) == "Invalid input!"
    assert str(close_time(-1, 200, "2017-01-01T00:00:00-08:00")) == "Invalid input!"
    assert str(open_time("abc", 200, "2017-01-01T00:00:00-08:00")) == "Invalid input!"
    assert str(close_time("abc", 200, "2017-01-01T00:00:00-08:00")) == "Invalid input!"
