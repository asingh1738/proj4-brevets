## Brevet time calculator

This application calculates brevet opening and closing times using Randonneurs USA specifications. https://rusa.org/pages/acp-brevet-control-times-calculator. The user enters starting time and date, selects the total brevet distance from the dropdown list, and the control point distance. The appllication will automatically populate the Open and Close fields with the proper times. The timezone used is 'US/Pacific'

* Using AJAX in the front end, the data for the starting date and time, total brevet distance (in km) and the control point distance (in either miles or km, automatically converts to km) is supplied.

* acp_times.py is where the two functions to calculate the times are located. Using the control point distance and the RUSA specified maximum and minimum speeds, the opening and closing times are calculated respectively. 
	* If data that isn't a positive number is supplied, the functions return "Invalid input!"
	* If the control distance is more than 20% greater than the total brevet distance, the functions return "Invalid input"
	* If the control distance is greater than the brevet distance, open_times will use the brevet distance in the calculation, and			  close_times will use the max time limits.
	* If a control distance is within 60km, close_times calculates the closing time with a speed of 20km/hr and adds 1 hour.
	* If the control distance is 0km, this is the start, and the closing time will be 1 hour after the specified start.

* There is an automated nose testing suite included. Navigate to the brevets directory and call "nosetests3"

Author: Arjun Singh
Contact: asingh7@uoregon.edu
Date: 11/08/2021